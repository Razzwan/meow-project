<?php

namespace App;

use Meow\Core\Application as App;

/**
 * Class Application for basic example ( for extending with trates )
 * @package App
 */
class Application extends App
{
    public function init()
    {
        //put your services here with registerService()
    }
}
