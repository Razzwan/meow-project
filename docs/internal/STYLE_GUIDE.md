Место для дискуссии [здесь](https://bitbucket.org/LifeIsWonderful/fuf/issues/4)

## Принято (4.10.2015г):
1. Полное соблюдение [PSR-2](http://www.php-fig.org/psr/psr-2/ru/)

2. Ниже описаны дополнения:

    * Объявление ```namespase``` сразу на следующей строке после ```<?php```
