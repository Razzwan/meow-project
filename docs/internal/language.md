# Открыто для [обсуждения](https://bitbucket.org/LifeIsWonderful/fuf/issues/5)


| Участик                        |    голос за язык  |  count |
| ------------------------------ | -----------------:| ------:|
| volter9                        |        en         |   2    |
| Илья Муравьев                  |        en         |   2    |
| Razzwan                        |        ru         |   2    |
| Artem Zzepish                  |        en         |   1    |
| Дмитрий Селивестров            |        ru         |   1    |
| Denis Batov                    |        ru         |   1    |



###Итого:

|язык | голосов|
| ----|-------:|
|ru | 4 |
|en | 5 |

