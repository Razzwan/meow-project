<?php
/**
 * phpunit tests/Http/DataBagTest
 */
use Meow\Http\DataBag;

/**
 * User: r
 * Date: 08.10.15
 * Time: 1:07
 */

class DataBagTest extends PHPUnit_Framework_TestCase
{
    protected $fixture;

    protected function setUp()
    {
        $this->fixture = null;
    }

    protected function tearDown()
    {
        $this->fixture = null;
    }

    public function data()
    {
        return [
            [
                'key' => 'key',
                'value' => 'value'
            ],
        ];
    }

    /**
     * @dataProvider data
     * @var $key string
     * @var $value string
     */
    public function testGet($key, $value)
    {
        $this->fixture = new DataBag([$key => $value]);
        $this->assertEquals($value, $this->fixture->get($key));
        $this->assertTrue($this->fixture->has($key));
        $this->assertEquals($value, $this->fixture->get($key));
    }
}
