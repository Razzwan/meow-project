<?php
/**
 * phpunit tests/Http/RequestTest
 */
use Meow\Http\Request;

/**
 * User: r
 * Date: 08.10.15
 * Time: 1:07
 */
class RequestTest extends PHPUnit_Framework_TestCase
{
    protected $fixture;

    protected function setUp()
    {
        $this->fixture = [];
    }

    protected function tearDown()
    {
        $this->fixture = NULL;
    }

    /**
     * @dataProvider Data
     * @param $get
     */
    public function testInitMethod($get)
    {
        $this->fixture[0] = new Request();
        $this->fixture[0]->init($get);
        $this->assertContains($get['page'], $this->fixture[0]->query->get('page'));
        $this->assertTrue($this->fixture[0]->query->has(array_keys($get)[0]));
    }


    public function Data()
    {
        return [
            'empty request' => [
                'GET' => ['page' => 'index'],
            ],

        ];
    }
}
