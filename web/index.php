<?php
/**
 * This is startup file of http product
 */

/**
 * Include autoloader
 */
include __DIR__ . '/../vendor/autoload.php';

ini_set('display_errors', 'on');
error_reporting(E_ALL | E_STRICT);

/**
 * @var $config array of configuration
 */
$config = require __DIR__ . '/../app/config/config.php';

/**
 * Create new application
 */
$app = new \App\Application();

/**
 * Configure application
 */
include __DIR__ . '/../app/bootstrap.php';

/**
 * Start application
 */
$app->run();
